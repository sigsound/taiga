	<footer id="footercover">

	<div class="contactcopy">
    	<div class="contactcopyL">
        Find out how TAIGA can express your story:<br>
        <a href="mailto:hello@wearetaiga.com?Subject=Hello%20again" target="_top">hello@wearetaiga.com</a><br></div>
        <br>
    	<div class="contactcopyS">
        Get in touch with our Principal Director, Aaron Rayburn:<br>
        <a href="tel:+503-568-1244">(503) 568-1244</a><br>
        <br>
        <a href="http://maps.google.com/?q=1200 NW 16th & Northrup, Portland, OR">NW 16th & Northrup in Portland, Oregon</a>
	</div>
</div>
	    <div class="footcopy"><p>©2013 TAIGA // ALL RIGHTS RESERVED // Site by: &nbsp; 
	    	<a href="http://fuzzinteractive.com">
		        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 	width="45px" height="11.85px" viewBox="150 360 300 79" enable-background="new 150 360 300 79" xml:space="preserve">
					<g>
						<polygon fill="#ffffff" points="150,360 150,439 170,439 170,420 170.001,420 170.001,420 190,420 190,400 170.001,400 
						170.001,380 450,380 450,360 	"/>
						<polygon fill="#ffffff" points="270.996,420.998 228.998,420.998 228.998,382 210.997,382 210.997,439 288.997,439 288.997,382 
							270.996,382 	"/>
						<polygon fill="#ffffff" points="349,382 290.997,382 290.997,399 330.997,399 330.997,439 389,439 389,420.998 349,420.998 	"/>
						<polygon fill="#ffffff" points="409.003,382 351,382 351,399 391.003,399 391.003,439 450,439 450,420.998 409.003,420.998 	"/>
					</g>
				</svg>
			</a>
		</div>
	</footer>
</div><!--#header-->

<?php
    wp_footer();
?>



<script>


jQuery(document).ready(function(){



	// Tree Nav

	jQuery("#nav").addClass("js").before('<div id="tree"><img src="<?php echo get_template_directory_uri(); ?>/images/greentree.png"></div>');
	jQuery("#tree").click(function(){
		jQuery("#nav").toggle();
	});
	jQuery(window).resize(function(){
		if(window.innerWidth > 768) {
			jQuery("#nav").removeAttr("style");
		}
	});


	// Flow Type


	jQuery('body').flowtype({
	 minimum   : 500,
	 maximum   : 1200,
	 minFont   : 12,
	 maxFont   : 40,
	 fontRatio : 30,
	 lineRatio : 1.45
	});



	//Fade in/out introcopy


    jQuery(window).scroll(function(){
        var posFromTop = jQuery(window).scrollTop();

        if(posFromTop > 800){
        // if more than 200px from the top do something
		jQuery("#introcopy").fadeIn(300);

        } else {
        // otherwise do something else.
        jQuery("#introcopy").fadeOut(300);

        }
    });


    //Smooth Scrolling

    jQuery('a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash,
	    jQuerytarget = jQuery(target);

	    jQuery('html, body').stop().animate({
	        'scrollTop': jQuerytarget.offset().top
	    }, 900, 'swing', function () {
	        window.location.hash = target;
	    });
	});


    //Top Animation 

     


});


		$(document).ready(function(){
			var $doc = $(document);
			var $win = $(window);

			// dimensions - we want to cache them on window resize
			var windowHeight, windowWidth;
			var fullHeight, scrollHeight;
			var streetImgWidth = 1024, streetImgHeight = 640;
			calculateDimensions();

			var currentPosition = -1, targetPosition = 0;
			var $videoContainer = $('.street-view');
			var video = $('.street-view > img')[0];
			var $hotspotElements = $('[data-position]');


			// handling resize and scroll events
			
			function calculateDimensions() {
				windowWidth = $win.width();
				windowHeight = $win.height();
				fullHeight = $('#main').height();
				scrollHeight = fullHeight - windowHeight;
			}
			
			function handleResize() {
				calculateDimensions();
				resizeBackgroundImage();
				handleScroll();
			}
			
			function handleScroll() {
				targetPosition = $win.scrollTop() / scrollHeight;
			}
			
			// main render loop
			window.requestAnimFrame = (function(){
			  return  window.requestAnimationFrame       ||
			          window.webkitRequestAnimationFrame ||
			          window.mozRequestAnimationFrame    ||
			          window.oRequestAnimationFrame      ||
			          window.msRequestAnimationFrame     ||
			          function(/* function */ callback, /* DOMElement */ element){
			            window.setTimeout(callback, 1000 / 60);
			          };
			})();


			function animloop(){
				if ( Math.floor(currentPosition*5000) != Math.floor(targetPosition*5000) ) {
					currentPosition += (targetPosition - currentPosition) / 5;
					render(currentPosition);
				}
			  requestAnimFrame(animloop);
			}

			




			// rendering


			function render( position ) {
				// position the elements
				var minY = -windowHeight, maxY = windowHeight;
				$.each($hotspotElements,function(index,element){
					var $hotspot = $(element);
					var elemPosition = Number( $hotspot.attr('data-position') );
					var elemSpeed = Number( $hotspot.attr('data-speed') );
					var elemY = windowHeight/2 + elemSpeed * (elemPosition-position) * scrollHeight;
					if ( elemY < minY || elemY > maxY ) {
						$hotspot.css({'visiblity':'none', top: '-1000px','webkitTransform':'none'});
					} else {
						$hotspot.css({'visiblity':'visible', top: elemY, position: 'fixed'});
					}
				});
				
				
				renderVideo( position );
			}



			function resizeBackgroundImage(){
				// get image container size
				var scale = Math.max( windowHeight/streetImgHeight , windowWidth/streetImgWidth );
				var width = scale * streetImgWidth , height = scale * streetImgHeight;
				var left = (windowWidth-width)/2, top = (windowHeight-height)/2;
				$videoContainer
						  .width(width).height(height)
						  .css('position','fixed')
						  .css('left',left+'px')
						  .css('top',top+'px');
			}





			// video handling

			var imageSeqLoader = new ProgressiveImageSequence( "http://wearetaiga.com/wp-content/themes/TAIGA/images/street/vid-{index}.jpg" , 100 , {
				indexSize: 4,
				initialStep: 16,
				onProgress: handleLoadProgress,
				onComplete: handleLoadComplete,
				stopAt: 1
			} );

			var loadCounterForIE = 0; // there seems to be a problem with ie calling the callback several times
			imageSeqLoader.loadPosition(currentPosition,function(){
				loadCounterForIE++;
				if ( loadCounterForIE == 1 ) {
					renderVideo(currentPosition);
					imageSeqLoader.load();
					imageSeqLoader.load();
					imageSeqLoader.load();
					imageSeqLoader.load();
				}
			});


			var currentSrc, currentIndex;

			function renderVideo(position) {
				var index = Math.round( currentPosition * (imageSeqLoader.length-1) );
				var img = imageSeqLoader.getNearest( index );
				var nearestIndex = imageSeqLoader.nearestIndex;
				if ( nearestIndex < 0 ) nearestIndex = 0;
				var $img = $(img);
				var src;
				if ( !!img ) {
					src = img.src;
					if ( src != currentSrc ) {
						video.src = src;
						currentSrc = src;
					}
				}
			}



			$('body').append('<div id="loading-bar" style="position:fixed; bottom:0; left:0; background-color: #DF0012; background-color: rgba(223,0,18,0.5); height: 1px;"></div>');
			
			function handleLoadProgress() {
				var progress = imageSeqLoader.getLoadProgress() * 100;
				$('#loading-bar').css({width:progress+'%',opacity:1});
			}

			function handleLoadComplete() {
				$('#loading-bar').css({width:'100%',opacity:0});
			}




			$win.resize( handleResize );
			$win.scroll( handleScroll );

			handleResize();
			animloop();




		});


</script>

	</body>
</html>