<?php

function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );

function scripts_load_cdn()
{
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js', array(), null, false );
	
	
	wp_register_script( 'Progressive', get_template_directory_uri() . '/js/ProgressiveImageSequence.class.js', array( 'jquery' ) );
	wp_enqueue_script( 'Progressive' );
    
    
	
	wp_register_script( 'FlowType', get_template_directory_uri() . '/js/flowtype.js', array( 'jquery' ) );
	wp_enqueue_script( 'FlowType' );
}
add_action( 'wp_enqueue_scripts', 'scripts_load_cdn' );

?>