<?php get_header(); ?>

<div id="content">
	<?php query_posts('post_type=page'); ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	 	<div class="videoWrapper" id= "video-<?php the_ID(); ?>">
	 	<?php the_content(); ?>
	 	</div>
	<?php endwhile; endif; ?>
</div>

<?php get_footer(); ?>
