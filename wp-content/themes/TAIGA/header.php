<!DOCTYPE html>  

<!--


//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
////	         
////	       ///             /// /////////////// ///////////////
////	       ///             /// /////////////// ///////////////
/////////      ///             ///             ///             ///
/////////      ///             ///             ///             ///   
////           ///             ///             ///             ///
////           ///             ///             ///             ///
////           ///////////////////             /////////////// ///////////////
////           ///////////////////             /////////////// ///////////////


-->
	
	<head>
		
		<title><?php wp_title('', true, 'right'); ?></title>
						
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
				
		<!-- media-queries.js (fallback) -->
		<!--[if lt IE 9]>
			<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>			
		<![endif]-->

		<!-- html5.js -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
  		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		
		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->
  		
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
		<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,700italic' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="//use.typekit.net/bdu7shn.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>



<script>
jQuery(document).ready(function(){
	var myDevice = jQuery(window).width()
	 if(myDevice < 800 ) {
    	jQuery('.royalSlider .rsImg').each(function() {
	        var element = $(this); // your IMG, or A tag (for lazy-loaded images)
	        var src = $(this).attr('src');
		
	        element.attr('src', src.replace('.jpg','-800x450.jpg')); 
   		});
   	}
 
 });
 </script>
        
	</head>
	
		<body>

			<div id="nav">
				<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
			</div>

			<div id="identity"><a class="scrolltrigger" href="#video-303"><img class="arrow" src="<?php echo get_template_directory_uri(); ?>/images/arrow.png" alt="more content below"></a>
			</div>

			<div id="introcopy">
				<h4>TAIGA creates original content, specializing in commercial and documentary film.</h4>
			</div>

			<div id="cover">
				<img src="<?php echo get_template_directory_uri(); ?>/images/mobilecover.jpg">
			</div>
			<div id="main" style="height: 2500px;">
				<div class="street-view">
					<img src="<?php echo get_template_directory_uri(); ?>/images/street/vid-0001.jpg">
				</div>
			</div>
			<div id="header">
